<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ArtistTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Тест добавления альбома артисту.
     */
    public function testAddAlbumToArtist(): void
    {
        $artistId = 1; // Предполагается, что у вас есть артист с ID 1
        $albumId = 2; // Предполагается, что у вас есть альбом с ID 2

        $response = $this->postJson("/api/artists/{$artistId}/albums", ['album_id' => $albumId]);

        $response->assertStatus(200);
        $response->assertJson(['message' => 'Альбом успешно добавлен']);
    }

    /**
     * Тест удаления альбома у артиста.
     */
    public function testDeleteAlbumFromArtist(): void
    {
        $artistId = 1; // Предполагается, что у вас есть артист с ID 1
        $albumId = 2; // Предполагается, что у вас есть альбом с ID 2

        $response = $this->deleteJson("/api/artists/{$artistId}/albums/{$albumId}");

        $response->assertStatus(200);
        $response->assertJson(['message' => 'Альбом успешно удален']);
    }

    /**
     * Тест добавления песни в альбом артиста.
     */
    public function testAddSongToAlbum(): void
    {
        $artistId = 1; // Предполагается, что у вас есть артист с ID 1
        $albumId = 2; // Предполагается, что у вас есть альбом с ID 2
        $songData = ['title' => 'Test Song', 'duration' => '3:30'];

        $response = $this->postJson("/api/artists/{$artistId}/albums/{$albumId}/songs", $songData);

        $response->assertStatus(200);
        $response->assertJson(['message' => 'Песня успешно добавлена']);
    }

    /**
     * Тест удаления песни из альбома артиста.
     */
    public function testDeleteSongFromAlbum(): void
    {
        $artistId = 1; // Предполагается, что у вас есть артист с ID 1
        $albumId = 2; // Предполагается, что у вас есть альбом с ID 2
        $songId = 3; // Предполагается, что у вас есть песня с ID 3

        $response = $this->deleteJson("/api/artists/{$artistId}/albums/{$albumId}/songs/{$songId}");

        $response->assertStatus(200);
        $response->assertJson(['message' => 'Песня успешно удалена']);
    }
}

