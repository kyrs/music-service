<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\RequestGuard;
use Illuminate\Support\Facades\Artisan;

class AuthenticationTest extends TestCase
{
    public function testUserCanLoginAndAccessProfileThenLogout()
    {
        // Отправляем POST-запрос на /api/login
        $response = $this->post('/api/login', [
            'email' => 'user@example.com',
            'password' => 'password',
        ]);

        // Проверяем, что ответ содержит токен
        $response->assertJsonStructure(['token']);

        // Извлекаем токен из ответа
        $token = $response->json()['token'];

        // Устанавливаем заголовок авторизации с полученным токеном
        $this->withHeaders(['Authorization' => 'Bearer '. $token])->get('/api/user');

        // Проверяем статус ответа на GET-запрос
        $response->assertStatus(200);

        // Дополнительная проверка: проверяем, что данные пользователя верны
        $response->assertJsonStructure([
            'id',
            'name',
            'email',
        ]);

        // Выходим из системы
        Auth::guard('api')->logout();

        // Проверяем, что после выхода из системы запрос к /api/user возвращает ошибку 401
        $response = $this->withHeaders(['Authorization' => 'Bearer '. $token])->get('/api/user');
        $response->assertStatus(401);

        Log::info('Тестирование завершено. Результаты:');
        Log::info('Тестирование входа и выхода пользователя: Успешно');
        Artisan::call('log:info', ['message' => 'Тестирование входа и выхода пользователя: Успешно']);
    }
}
