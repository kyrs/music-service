<?php

namespace Tests\Feature;

use App\Models\Response;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;

class MonitorApiResponseTimeTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function monitor_api_response_time_records_correct_data()
    {
        // Создание фиктивного запроса с задержкой ответа
        $response = Response::factory()->create([
            'time' => 500, // Задержка в 500 мс
        ]);

        // Выполнение задачи мониторинга времени ответа
        $this->artisan('monitor:api-response-time');

        // Проверка, что данные о времени ответа были записаны
        $updatedResponse = Response::find($response->id);
        $this->assertEquals(500, $updatedResponse->time);

        Log::info('Updated response time: '. $updatedResponse->time);
        Log::info('Finished test: monitor_api_response_time_records_correct_data');
    }
}
