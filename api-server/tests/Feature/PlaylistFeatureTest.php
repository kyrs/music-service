<?php

namespace Tests\Feature;

use App\Models\Artist;
use App\Models\Album;
use App\Models\Playlist;
//use App\Models\Genres;
use App\Models\Song;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PlaylistFeatureTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_can_create_a_playlist_with_user_and_songs()
    {
        // Создаем пользователя
        $user = User::create([
            'name' => 'Test User',
            'email' => 'test@example.com',
            'password' => bcrypt('password'),
        ]);

        // Обновляем последнюю активность пользователя
        $user->updateLastActivity();

        // Создаем артиста для альбома
        $artist = Artist::create([
            'name' => 'Test Artist',
            'email' => 'artist@example.com',
            'password' => bcrypt('artistpassword'),
        ]);

        // Создаем жанр для альбома
        /*$genre = Genre::create([
            'name' => 'Test Genre',
        ]);*/

        // Создаем альбом для песни
        $album = Album::create([
            'name' => 'Test Album',
            'artist_id' => $artist->id,
            //'genre_id' => $genre->id,
        ]);

        // Создаем песню, связанную с альбомом
        $song = Song::create([
            'name' => 'Test Song',
            'album_id' => $album->id,
            'duration' => 300,
        ]);

        // Создаем плейлист, связанный с пользователем
        $playlist = Playlist::create([
            'name' => 'Test Playlist',
            'description' => 'This is a test playlist',
            'user_id' => $user->id,
        ]);

        // Добавляем песню в плейлист
        $playlist->songs()->attach($song->id);

        // Проверяем, что плейлист был успешно сохранен
        $this->assertDatabaseHas('playlists', [
            'name' => 'Test Playlist',
            'description' => 'This is a test playlist',
            'user_id' => $user->id,
        ]);

        // Проверяем, что песня была успешно добавлена в плейлист
        $this->assertDatabaseHas('playlist_song', [
            'playlist_id' => $playlist->id,
            'song_id' => $song->id,
        ]);
    }
}
