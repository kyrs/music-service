<?php

declare(strict_types=1);

namespace Tests\Feature;

use App\Models\Album;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AlbumTest extends TestCase
{
    use RefreshDatabase;

    public function testShouldCreateAlbum(): void
    {
        $response = $this->json('POST', 'api/albums', ['name' => 'Album #1', 'genreId' => 1]);

        $response->assertStatus(201);

        $albums = Album::query()->get();
        $this->assertCount(1, $albums);

        \Log::info("Test ShouldCreateAlbum result: Response status - {$response->status()}, Albums count - {$albums->count()}");
    }

    public function testShouldNotCreateAlbumDueToValidation(): void
    {
        $response = $this->json('POST', 'api/albums', ['genreId' => 1]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['name']);

        $albums = Album::query()->get();
        $this->assertCount(0, $albums);

        \Log::info("Test ShouldNotCreateAlbumDueToValidation result: Response status - {$response->status()}, Albums count - {$albums->count()}");
    }
}
