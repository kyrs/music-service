<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;

class MonitorActiveUsersTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function monitor_active_users_counts_users()
    {
        // Создание двух активных пользователей
        User::factory()->count(2)->create();

        // Выполнение задачи мониторинга активных пользователей
        $this->artisan('monitor:active-users');

        // Проверка, что количество активных пользователей увеличилось
        $activeUsersCount = User::where('active', true)->count();
        $this->assertEquals(2, $activeUsersCount);

        Log::info('Checked active users count. Expected: 2, Actual: '. $activeUsersCount);
    }
}
