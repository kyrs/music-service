<?php

use App\Http\Controllers\AlbumController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\SongController;
use App\Http\Controllers\PlaylistController;
use App\Http\Middleware\Authenticate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\FavoriteSongController;
use App\Http\Controllers\LikeController;
use App\Http\Controllers\RatingController;
use App\Http\Controllers\ArtistController;
use App\Http\Controllers\MonitoringController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::middleware(Authenticate::class)->post('logout', [AuthController::class, 'logout']);
    Route::middleware(Authenticate::class)->post('refresh', [AuthController::class, 'refresh']);
    Route::middleware(Authenticate::class)->get('me', [AuthController::class, 'me']);
});

Route::group(['prefix' => 'albums', 'middleware' => Authenticate::class], function () {
    Route::middleware(HavePermission::permissions('create-albums'))
        ->put('', [AlbumController::class, 'createAlbum']);
    Route::put('{albumId}', [AlbumController::class, 'updateAlbum']);
    Route::delete('{albumId}', [AlbumController::class, 'deleteAlbum']);

    Route::get('{albumId}', [AlbumController::class, 'getAlbumById']);

    Route::group(['prefix' => '{albumId}/songs'], function () {
        Route::post('', [SongController::class, 'uploadSong']);
        Route::get('', [SongController::class, 'getSongsOfAlbum']);
    });
});

Route::get('/api/playlists', [PlaylistController::class, 'index']);

Route::group(['prefix' => 'playlists', 'middleware' => Authenticate::class], function () {
    Route::get('/', [PlaylistController::class, 'index']);
    Route::post('/', [PlaylistController::class, 'store']);
    Route::get('/{playlist}', [PlaylistController::class, 'show']);
    Route::put('/{playlist}', [PlaylistController::class, 'update']);
    Route::delete('/{playlist}', [PlaylistController::class, 'destroy']);
});

Route::apiResource('comments', CommentController::class);
Route::apiResource('favorite-songs', FavoriteSongController::class);
Route::apiResource('likes', LikeController::class);
Route::apiResource('ratings', RatingController::class);

Route::get('/api/artists', [ArtistController::class, 'index']);
Route::get('/api/albums', [AlbumController::class, 'index']);
Route::get('/api/songs', [SongController::class, 'index']);

Route::prefix('artists')->group(function () {
    Route::post('/{artistId}/albums', [ArtistController::class, 'addAlbumToArtist']);
    Route::delete('/{artistId}/albums/{albumId}', [ArtistController::class, 'deleteAlbumFromArtist']);
    Route::post('/{artistId}/albums/{albumId}/songs', [ArtistController::class, 'addSongToAlbum']);
    Route::delete('/{artistId}/albums/{albumId}/songs/{songId}', [ArtistController::class, 'deleteSongFromAlbum']);
});

Route::get('/monitor', [MonitoringController::class, 'monitor']);

// Для MonitoringResult
Route::get('/monitoring-result', [MonitoringResultController::class, 'index']);

// Для MonitoringLog
Route::get('/monitoring-log', [MonitoringLogController::class, 'index']);


