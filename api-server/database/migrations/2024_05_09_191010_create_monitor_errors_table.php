<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
{
    Schema::create('monitor_errors', function (Blueprint $table) {
        $table->id();
        $table->integer('error_count');
        $table->timestamp('timestamp');
        $table->string('error_message');
        $table->string('error_type');
        $table->unsignedBigInteger('user_id')->nullable();
        $table->unsignedBigInteger('request_id')->nullable();
        $table->timestamps();
    });
}

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('monitor_errors');
    }
};
