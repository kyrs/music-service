<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
{
    Schema::create('monitoring_results', function (Blueprint $table) {
        $table->id();
        $table->timestamp('timestamp');
        $table->integer('request_count');
        $table->integer('error_count');
        $table->float('api_response_time');
        $table->integer('user_count');
        $table->timestamps();
    });
}

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('monitoring_results');
    }
};
