FROM php:8.2-cli-alpine

RUN docker-php-ext-install pdo_mysql

RUN (crontab -l; echo "*/30 * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1") | crontab -

RUN (crontab -l; echo "" ) | crontab -

CMD [ "/usr/sbin/crond", "-f", "-d8" ]
