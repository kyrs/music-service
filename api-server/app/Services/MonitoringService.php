<?php

namespace App\Services;

use App\Models\MonitoringResult;
use App\Models\MonitoringLog;
use App\Models\MonitorRequest; // Предполагается, что у вас есть модель MonitorRequest
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class MonitoringService {
    const MONITORING_RESULTS_TABLE = 'monitoring_results';

    public function monitorRequests() {
        $requestCount = MonitorRequest::count();
        return $requestCount;
    }

    public function monitorApiResponseTime() {
        $apiResponseTime = MonitorRequest::where('request_type', 'API')
            ->selectRaw('avg(response_time)')
            ->value('response_time');
        return $apiResponseTime;
    }

    public function monitorActiveUsers() {
        $userCount = MonitorRequest::where('user_type', 'active')->count();
        return $userCount;
    }

    public function monitorErrors() {
        $errorCount = MonitorRequest::whereNotNull('error_message')->count();
        return $errorCount;
    }

    public function recordMonitoringResults($requestCount, $errorCount, $apiResponseTime, $userCount, $pageLoadTime) {
        if ($requestCount > 0 && $errorCount > 0 && $apiResponseTime > 0 && $userCount > 0) {
            try {
                DB::table(self::MONITORING_RESULTS_TABLE)->insert([
                    'timestamp' => now(),
                    'request_count' => $requestCount,
                    'error_count' => $errorCount,
                    'api_response_time' => $apiResponseTime,
                    'user_count' => $userCount,
                ]);
            } catch (\Exception $e) {
                // Обработка исключения
                // Например, логирование ошибки
                Log::error("Failed to record monitoring results: ". $e->getMessage());
            }

        }
    }
}
