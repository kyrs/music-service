<?php

declare(strict_types=1);

namespace App\Services;

use App\DataAccess\Repositories\PlaylistRepository;
use App\Models\Playlist;
use Illuminate\Support\Collection;

class PlaylistService
{
    public function __construct(
        private readonly PlaylistRepository $playlistRepository
    ) {
    }

    public function getPlaylistsForUser(int $userId): Collection
    {
        return $this->playlistRepository->getPlaylistsByUser($userId);
    }

    public function createPlaylist(int $userId, string $name, ?string $description = null): int
    {
        return $this->playlistRepository->createPlaylist($userId, $name, $description);
    }

    public function getPlaylistById(int $playlistId): Playlist|null
    {
        return $this->playlistRepository->getById($playlistId);
    }

    public function updatePlaylist(int $playlistId, array $data): Playlist|null
    {
        return $this->playlistRepository->updatePlaylist($playlistId, $data);
    }

    public function deletePlaylist(int $playlistId): bool
    {
        $this->playlistRepository->deletePlaylist($playlistId);
        return true; // Предполагается, что удаление прошло успешно
    }

    public function getSongsInPlaylist(int $playlistId): Collection
    {
        return $this->playlistRepository->getSongsInPlaylist($playlistId);
    }
}
