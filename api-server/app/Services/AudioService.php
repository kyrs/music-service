<?php

declare(strict_types=1);

namespace App\Services;

use App\DataAccess\Repositories\AlbumRepository;
use App\DataAccess\Repositories\SongRepository;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;

class AudioService
{
    public function __construct(
        private readonly AlbumRepository $albumRepository,
        private readonly SongRepository $songRepository,
        private readonly StorageService $storageService
    ) {
    }

    /*public function saveAudio(int $albumId, string $title, UploadedFile $file): int
    {
        $album = $this->albumRepository->getById($albumId);

        $filePath = $this->storageService->storeAudio($album->cdn_folder_id, $file);

        return $this->songRepository->createSong($albumId, $title, 0, $filePath)->id;
    }*/
    public function saveAudio(SongDTO $songDTO): int
    {
        $album = $this->albumRepository->getById($songDTO->albumId);

        $filePath = $this->storageService->storeAudio($album->cdn_folder_id, $songDTO->file);

        return $this->songRepository->createSong($songDTO->albumId, $songDTO->title, $songDTO->duration, $filePath)->id;
    }

    public function getSongsByAlbum(int $albumId): Collection
    {
        return $this->songRepository->getSongsByAlbum($albumId);
    }

    public function searchSongs(string $query): Collection
    {
        return $this->songRepository->searchSongs($query);
    }

    /*public function updateSong(int $songId, array $data): Song|null
    {
        return $this->songRepository->updateSong($songId, $data);
    }*/

    public function updateSong(int $songId, SongDTO $songDTO): Song|null
    {
    // Предполагается, что updateSong в SongRepository принимает SongDTO
        return $this->songRepository->updateSong($songId, $songDTO);
    }

    public function deleteSong(int $songId): bool
    {
        return $this->songRepository->deleteSong($songId);
    }
}
