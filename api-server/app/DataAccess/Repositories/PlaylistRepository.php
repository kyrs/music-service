<?php

declare(strict_types=1);

namespace App\DataAccess\Repositories;

use App\Models\Playlist;
use Illuminate\Support\Collection;

class PlaylistRepository
{
    public function createPlaylist(int $userId, string $name, ?string $description = null): int
    {
        $playlist = new Playlist();
        $playlist->user_id = $userId;
        $playlist->name = $name;
        $playlist->description = $description;

        $playlist->save();

        return $playlist->id;
    }

    public function getById(int $id): Playlist|null
    {
        return Playlist::query()->where('id', '=', $id)->first();
    }

    public function searchPlaylists(string $query): Collection
    {
        return Playlist::query()
            ->where('name', 'like', '%' . $query . '%')
            ->orWhere('description', 'like', '%' . $query . '%')
            ->get();
    }

    public function getPlaylistsByUser(int $userId): Collection
    {
        return Playlist::query()->where('user_id', '=', $userId)->get();
    }

    public function getSongsInPlaylist(int $playlistId): Collection
    {
        $playlist = Playlist::find($playlistId);
        if ($playlist) {
            return $playlist->songs;
        }
        return new Collection();
    }

    public function addSongToPlaylist(int $playlistId, int $songId): bool
    {
        $playlist = Playlist::find($playlistId);
        if ($playlist) {
        $playlist->songs()->attach($songId);
           return true;
        }
           return false;
    }

    public function removeSongFromPlaylist(int $playlistId, int $songId): bool
    {
        $playlist = Playlist::find($playlistId);
        if ($playlist) {
        $playlist->songs()->detach($songId);
           return true;
        }
           return false;
    }

    public function getPlaylistsForSong(int $songId): Collection
    {
        $song = Song::find($songId);
        if ($song) {
           return $song->playlists;
        }
           return new Collection();
    }

    public function updatePlaylist(int $id, array $data): Playlist|null
    {
        $playlist = Playlist::query()->where('id', '=', $id)->first();
        if ($playlist) {
            $playlist->fill($data);
            $playlist->save();
            return $playlist;
        }
        return null;
    }

    public function deletePlaylist(int $id): void
    {
        Playlist::query()->where('id', '=', $id)->delete();
    }
}
