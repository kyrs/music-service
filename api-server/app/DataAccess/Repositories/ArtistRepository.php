<?php

declare(strict_types=1);

namespace App\DataAccess\Repositories;

use App\Models\Artist;
use App\Models\Album;
use App\Models\Song;

class ArtistRepository
{
    public function getByEmail(string $email): Artist|null
    {
        return Artist::query()
            ->where('email', '=', $email)
            ->first();
    }

    public function addAlbumToArtist(int $artistId, int $albumId): bool
    {
        $artistAlbum = new ArtistAlbum();
        $artistAlbum->artist_id = $artistId;
        $artistAlbum->album_id = $albumId;

        return $artistAlbum->save();
    }

    public function getArtistAlbums(int $artistId): \Illuminate\Database\Eloquent\Collection
    {
        return Artist::find($artistId)->albums;
    }

    public function createArtist(array $attributes): Artist
    {
        return Artist::create($attributes);
    }

    public function updateArtist(Artist $artist, array $attributes): bool
    {
        return $artist->update($attributes);
    }

    public function deleteArtist(Artist $artist): bool
    {
        return $artist->delete();
    }

    // Удаление альбома у артиста
    public function deleteAlbumFromArtist(int $artistId, int $albumId): bool
    {
        // Предполагается, что у вас есть метод в модели Album для удаления связи с артистом
        $album = Album::find($albumId);
        if ($album && $album->artists()->detach($artistId)) {
            return true;
        }
        return false;
    }

    // Добавление песни в альбом артиста
    public function addSongToAlbum(int $artistId, int $albumId, array $songData): bool
    {
        // Предполагается, что у вас есть метод в модели Song для создания новой песни
        $song = new Song($songData);
        $song->album_id = $albumId;

        return $song->save();
    }

    // Удаление песни из альбома артиста
    public function removeSongFromAlbum(int $artistId, int $albumId, int $songId): bool
    {
        // Предполагается, что у вас есть метод в модели Song для удаления песни
        $song = Song::find($songId);
        if ($song && $song->delete()) {
            return true;
        }
        return false;
    }
}
