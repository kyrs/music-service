<?php

declare(strict_types=1);

namespace App\DataAccess\Repositories;

use App\DTO\AlbumDTO;
use App\DTO\MappingSchemes\AlbumMappingScheme;
use App\Models\Album;
use Illuminate\Support\Collection;

class AlbumRepository
{
    public function createAlbum(int $artistId, string $name, int $genreId): int
    {
        $album = new Album();
        $album->artist_id = $artistId;
        $album->name = $name;
        $album->genre_id = $genreId;
        $album->cdn_folder_id = uniqid(more_entropy: true);

        $album->save();

        return $album->id;
    }

    public function getById(int $id): Album|null
    {
        return Album::query()->where('id', '=', $id)->first();
    }

    public function searchAlbums(string $query): Collection
    {
        return Album::query()
            ->where('name', 'like', '%' . $query . '%')
            ->orWhere('artist_name', 'like', '%' . $query . '%')
            ->orWhere('genre_name', 'like', '%' . $query . '%')
            ->get();
    }

    public function getAlbumsByArtist(int $artistId): Collection
    {
        return Album::query()->where('artist_id', '=', $artistId)->get();
    }

    public function getSongsInAlbum(int $albumId): Collection
    {
        return Album::find($albumId)->songs;
    }

    public function updateAlbum(int $id, array $data): Album|null
    {
        $album = Album::query()->where('id', '=', $id)->first();
    if ($album) {
        $album->fill($data);
        $album->save();
        return $album;
    }
        return null;
    }

    public function deleteAlbum(int $id): void
    {
        Album::query()->where('id', '=', $id)->delete();
    }

    public function getAlbumDTOById(int $id): ?AlbumDTO
    {
        $album = $this->getById($id);

        if (!$album) {
            return null;
        }

        // Преобразование данных из формата базы данных в DTO
        $mappingScheme = AlbumMappingScheme::getScheme();
        $transformedData = [];

        foreach ($mappingScheme as $mappingItem) {
            $transformedData[$mappingItem->getDtoField()] = $album->{$mappingItem->getDatabaseField()};
        }

        // Создание и возврат DTO
        return new AlbumDTO(
            $transformedData['title'],
            $transformedData['artist'],
            $transformedData['releaseDate'],
            $transformedData['songs'] ?? []
        );
    }
}

