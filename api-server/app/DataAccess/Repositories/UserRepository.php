<?php

declare(strict_types=1);

namespace App\DataAccess\Repositories;

use App\Models\User;
use App\DTO\AlbumDTO;

class UserRepository
{
    public function getByEmail(string $email): User|null
    {
        return User::query()
            ->where('email', '=', $email)
            ->first();
    }

    /*public function addAlbumToUser(int $userId, int $albumId): bool
    {
        $userAlbum = new UserAlbum();
        $userAlbum->user_id = $userId;
        $userAlbum->album_id = $albumId;

        return $userAlbum->save();
    }*/

    public function addAlbumToUser(AlbumDTO $albumDTO): bool
    {
        $userAlbum = new UserAlbum();
        $userAlbum->user_id = $albumDTO->userId; // Предполагается, что в AlbumDTO есть свойство userId
        $userAlbum->album_id = $albumDTO->albumId; // Предполагается, что в AlbumDTO есть свойство albumId

        return $userAlbum->save();
    }

    public function createAlbum(AlbumDTO $albumDTO): Album
    {
        $album = new Album();
        $album->title = $albumDTO->title;
        $album->artist = $albumDTO->artist;
        $album->release_date = $albumDTO->releaseDate;
    // Дополнительная логика для обработки списка песен из $albumDTO->songs

        $album->save();

        return $album;
    }

    public function getUserAlbums(int $userId): array
    {
         $albums = Album::where('user_id', $userId)->get();
         $albumDTOs = [];

        foreach ($albums as $album) {
        $albumDTO = new AlbumDTO(
            $album->title,
            $album->artist,
            $album->release_date,
            $songs->songs,
            $userId->$userId,
            $albumId = $albumId,
            // Преобразование списка песен в нужный формат
        );
            $albumDTOs[] = $albumDTO;
            }
        return $albumDTOs;
    }

        public function updateAlbum(int $albumId, AlbumDTO $albumDTO): Album
        {
             $album = Album::find($albumId);
             $album->title = $albumDTO->title;
             $album->artist = $albumDTO->artist;
             $album->release_date = $albumDTO->releaseDate;
            // Дополнительная логика для обновления списка песен

            $album->save();

        return $album;
    }
}


