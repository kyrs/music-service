<?php

declare(strict_types=1);

namespace App\DataAccess\Repositories;

use App\DTO\SongDTO;
use App\DTO\MappingSchemes\SongMappingScheme;
use App\Models\Song;
use Illuminate\Support\Collection;

class SongRepository
{
    /**
     * @return Collection<int, Song>
     */
    public function getSongsByAlbum(int $albumId): Collection
    {
        return Song::query()->where('album_id', '=', $albumId)->get();
    }

    public function createSong(int $albumId, string $name, int $duration, string $filePath): Song
    {
        $song = new Song();
        $song->album_id = $albumId;
        $song->name = $name;
        $song->duration = $duration;
        $song->path = $filePath;

        $song->save();

        return $song;
    }

    public function searchSongs(string $query): Collection
    {
        return Song::query()
            ->where('name', 'like', '%' . $query . '%')
            ->orWhere('artist', 'like', '%' . $query . '%')
            ->get();
    }

    /*public function updateSong(int $id, array $data): Song|null
    {
        $song = Song::query()->where('id', '=', $id)->first();
    if ($song) {
        $song->fill($data);
        $song->save();

        return $song;
        }

        return null;
    }*/

    public function updateSong(int $id, SongDTO $songDTO): Song|null
    {
        $song = Song::query()->where('id', '=', $id)->first();
    if ($song) {
        // Преобразование данных из SongDTO в массив для обновления модели
        $data = [
            'name' => $songDTO->title,
            'artist' => $songDTO->artist,
            'duration' => $songDTO->duration,
            'genre' => $songDTO->genre,
            'album' => $songDTO->album,
        ];

        $song->fill($data);
        $song->save();

        return $song;
    }
    return null;
    }


    public function deleteSong(int $songId): bool
    {
        $song = Song::find($songId);
    if ($song) {
        return $song->delete();
    }
        return false;
    }

    /**
     * Получить песни альбома в формате DTO.
     *
     * @param int $albumId ID альбома.
     * @return Collection<int, SongDTO>
     */
    public function getSongsByAlbumDTO(int $albumId): Collection
    {
        $songs = $this->getSongsByAlbum($albumId);

        return $songs->map(function (Song $song) {
            $mappingScheme = SongMappingScheme::getScheme();
            $transformedData = [];

            foreach ($mappingScheme as $mappingItem) {
                $transformedData[$mappingItem->getDtoField()] = $song->{$mappingItem->getDatabaseField()};
            }

            // Создание и возврат DTO
            return new SongDTO(
                $transformedData['title'],
                $transformedData['artist'],
                $transformedData['duration'],
                $transformedData['genre'],
                $transformedData['album']
            );
        });
    }
}

