<?php

declare(strict_types=1);

namespace App\Utils;

use BadFunctionCallException;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use InvalidArgumentException;
use stdClass;
use UnitEnum;
use App\DTO\MappingSchemes\AlbumMappingScheme;
use App\DTO\MappingSchemes\CommentMappingScheme;
use App\DTO\MappingSchemes\PlaylistMappingScheme;
use App\DTO\MappingSchemes\RatingMappingScheme;
use App\DTO\MappingSchemes\SongMappingScheme;

// todo: make possible to traverse complex properties

class Mapper
{
    private object $source;

    private array $includeProperties = [];

    private array $excludeProperties = [];

    private bool $includeUsed = false;

    private bool $excludeUsed = false;

    private bool $emptyUsed = false;

    private bool $toEmpty = false;

    private bool $useDefaultScheme = false;

    private bool $defaultSchemeUsed = false;

    private bool $toCamel = false;

    private bool $toSnake = false;

    private bool $caseUsed = false;

    private bool $usingManualFieldMapping = false;

    private array $manualMappingFieldsProperties = [];

    private function __construct(object $source)
    {
        $this->source = $source;
    }

    public static function from(object $source, bool $checkPersisting = true): Mapper
    {
        if ($checkPersisting && self::isEloquentModel($source) && !$source->exists) {
            throw new InvalidArgumentException(
                'source based on the Eloquent must be loaded from the database before mapping'
            );
        }

        return new self($source);
    }

    public function defaultScheme(): Mapper
    {
        $this->useDefaultScheme = true;

        if ($this->defaultSchemeUsed) {
            throw new BadFunctionCallException(
                'default scheme has already been applied and cannot be applied again'
            );
        }

        $this->defaultSchemeUsed = true;

        return $this;
    }

    public function emptyIfNotPresent(): Mapper
    {
        $this->toEmpty = true;

        if ($this->emptyUsed) {
            throw new BadFunctionCallException(
                'empty rule has already been set and cannot be used again'
            );
        }

        $this->emptyUsed = true;

        return $this;
    }

    public function exclude(array $properties): Mapper
    {
        if ($this->includeUsed) {
            throw new BadFunctionCallException(
                'include rule has already been set and cannot be used along with exclude'
            );
        }

        if ($this->excludeUsed) {
            $this->excludeProperties = array_merge($this->excludeProperties, $properties);
        } else {
            $this->excludeProperties = $properties;
        }

        $this->excludeUsed = true;

        return $this;
    }

    public function excludeFrom(string $className): Mapper
    {
        return $this->exclude(array_keys(get_class_vars($className)));
    }

    public function include(array $properties): Mapper
    {
        if ($this->excludeUsed) {
            throw new BadFunctionCallException(
                'exclude rule has already been set and cannot be used along with include'
            );
        }

        $this->includeProperties = $properties;
        $this->includeUsed = true;

        return $this;
    }

    public function toSnakeCase(): Mapper
    {
        $this->toSnake = true;

        if ($this->caseUsed) {
            throw new BadFunctionCallException('case conversion rule has already been set');
        }

        $this->caseUsed = true;

        return $this;
    }

    public function toCamelCase(): Mapper
    {
        $this->toCamel = true;

        if ($this->caseUsed) {
            throw new BadFunctionCallException('case conversion rule has already been set');
        }

        $this->caseUsed = true;

        return $this;
    }

    /**
     * @template T of object
     * @param T $destination
     * @return T
     */
    public function mapTo(object $destination)
    {
        /** @var MappingSchemeItem[] $scheme */
        $scheme = [];

        $scheme[] = new AlbumMappingScheme();
        $scheme[] = new CommentMappingScheme();
        $scheme[] = new PlaylistMappingScheme();
        $scheme[] = new RatingMappingScheme();
        $scheme[] = new SongMappingScheme();

        if (self::isEloquentModel($this->source)) {
            $sourceVars = $this->source->toArray();

            foreach ($this->source->getDates() as $dateField) {
                $sourceVars[$dateField] = $this->source->$dateField;
            }

            foreach ($this->source->getCasts() as $fieldName => $type) {
                if (!isset($this->source->$fieldName) || !array_key_exists($fieldName, $sourceVars)) {
                    continue;
                }

                $sourceVars[$fieldName] = $this->source->$fieldName;
            }
        } else {
            $sourceVars = get_object_vars($this->source);
        }

        if ($this->includeProperties) {
            $sourceVars = self::array_filter_key(
                $sourceVars,
                fn($key) => in_array($key, $this->includeProperties, true)
            );
        } elseif ($this->excludeProperties) {
            $sourceVars = self::array_filter_key(
                $sourceVars,
                fn($key) => !in_array($key, $this->excludeProperties, true)
            );
        }

        if (!$this->caseUsed) {
            if (self::isEloquentModel($this->source)) {
                if (!self::isEloquentModel($destination)) {
                    $this->toCamelCase();
                }
            } elseif (self::isEloquentModel($destination)) {
                $this->toSnakeCase();
            }
        }

        if ($this->caseUsed) {
            $keys = array_map(
                function ($key) {
                    if ($this->toCamel) {
                        return Str::camel($key);
                    }

                    if ($this->toSnake) {
                        return Str::snake($key);
                    }

                    return $key;
                },
                array_keys($sourceVars)
            );

            $sourceVars = array_combine($keys, array_values($sourceVars));
        }

        $destinationVars = get_class_vars(get_class($destination));

        if (self::isEloquentModel($destination)) {
            if ($destination->exists) {
                $destinationVars = $destination->toArray();
            } else {
                $destinationVars = $sourceVars;
            }
        }

        foreach ($destinationVars as $key => $value) {
            $sourceKey = $this->getSourceKey($scheme, $key);

            if (!is_string($sourceKey) && is_callable($sourceKey)) {
                $destination->$key = $sourceKey();

                continue;
            }

            if (array_key_exists($sourceKey, $sourceVars)) {
                if (
                    $value !== null && !is_scalar($value)
                    && !($value instanceof Carbon) && !($value instanceof UnitEnum)
                ) {
                    continue;
                }

                $destination->$key = $sourceVars[$sourceKey];

                continue;
            }

            if ($this->toEmpty) {
                $destination->$key = new stdClass();
            }
        }

        return $destination;
    }

    /** the key indicates from which field, to which value
     * @param array<string, string> $mapFields
     * @return $this|Mapper
     */
    public function mapFields(array $mapFields): Mapper
    {
        if ($this->usingManualFieldMapping) {
            throw new BadFunctionCallException(
                'extended map rule has already been set and cannot be used along with include'
            );
        }

        $this->manualMappingFieldsProperties = $mapFields;
        $this->usingManualFieldMapping = true;

        return $this;
    }

    private static function isEloquentModel(object $obj): bool
    {
        return is_subclass_of($obj, Model::class);
    }

    /**
     * @param MappingSchemeItem[] $scheme
     * @param string $destinationKey
     * @return string|callable
     */
    private function getSourceKey(array $scheme, string $destinationKey)
    {
        if (
            $this->usingManualFieldMapping
            && array_key_exists($destinationKey, $this->manualMappingFieldsProperties)
            && $this->manualMappingFieldsProperties[$destinationKey] !== null
        ) {
            return $this->manualMappingFieldsProperties[$destinationKey];
        }

        if (count($scheme) === 0) {
            return $destinationKey;
        }

        foreach ($scheme as $schemeItem) {
            if ($schemeItem->to === $destinationKey) {
                return $schemeItem->from;
            }
        }

        return $destinationKey;
    }

    private static function array_filter_key(array $array, callable $callback): array
    {
        $matchedKeys = array_filter(array_keys($array), $callback);

        return array_intersect_key($array, array_flip($matchedKeys));
    }
}
