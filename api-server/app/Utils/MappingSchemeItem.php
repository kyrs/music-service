<?php

declare(strict_types=1);

namespace App\Utils;

class MappingSchemeItem
{
    public string $from;

    public string $to;

    public function __construct(string $from, string $to)
    {
        $this->from = $from;
        $this->to = $to;
    }
}
