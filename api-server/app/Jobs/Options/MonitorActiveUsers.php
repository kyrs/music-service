<?php

namespace App\Jobs\Options;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class MonitorActiveUsers implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of active users to check.
     *
     * @var int
     */
    private $numUsers;

    /**
     * Create a new job instance.
     *
     * @param int $numUsers The number of active users to check.
     */
    public function __construct(int $numUsers = 10)
    {
        $this->numUsers = $numUsers;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        // Measure the start time
        $startTime = microtime(true);

        // Query the database to get the number of active users
        $activeUsers = DB::table('sessions')
            ->where('last_activity', '>', now()->subMinutes(5)) // Adjust the timeframe as needed
            ->count();

        // Calculate the elapsed time
        $elapsedTime = microtime(true) - $startTime;

        // Log the result
        \Log::info("Checked {$this->numUsers} active users in {$elapsedTime} seconds");
    }
}
