<?php

namespace App\Jobs\Options;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;

class MonitorApiResponseTime implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The URL of the API endpoint to monitor.
     *
     * @var string
     */
    private $apiEndpoint;

    /**
     * Create a new job instance.
     *
     * @param string $apiEndpoint The URL of the API endpoint to monitor.
     */
    public function __construct(string $apiEndpoint)
    {
        $this->apiEndpoint = $apiEndpoint;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        // Measure the start time
        $startTime = microtime(true);

        // Send a request to the API endpoint
        $response = Http::get($this->apiEndpoint);

        // Calculate the elapsed time
        $elapsedTime = microtime(true) - $startTime;

        // Log the result
        \Log::info("API Response Time: {$elapsedTime} seconds");
    }
}
