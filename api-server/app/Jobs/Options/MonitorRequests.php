<?php

namespace App\Jobs\Options;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class MonitorRequests implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of requests to simulate.
     *
     * @var int
     */
    private $numRequests;

    /**
     * Create a new job instance.
     *
     * @param int $numRequests The number of requests to simulate.
     */
    public function __construct(int $numRequests = 100)
    {
        $this->numRequests = $numRequests;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        // Measure the start time
        $startTime = microtime(true);

        // Simulate processing requests
        for ($i = 0; $i < $this->numRequests; $i++) {
            DB::table('your_table')->insert([
                'column1' => 'value1',
                'column2' => 'value2',
                // Add other columns as needed
            ]);
        }

        // Calculate the elapsed time
        $elapsedTime = microtime(true) - $startTime;

        // Log the result
        \Log::info("Processed {$this->numRequests} requests in {$elapsedTime} seconds");
    }
}
