<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MonitoringResult extends Model
{
    use HasFactory;

    protected $fillable = [
        'timestamp',
        'request_count',
        'error_count',
        'api_response_time',
        'user_count'
    ];

    public function getSummary()
    {
        return "Запросов: {$this->request_count}, Ошибок: {$this->error_count}, Среднее время ответа: {$this->api_response_time}, Активных пользователей: {$this->user_count}";
    }

    public function getTrend()
    {
        // Реализация анализа тренда
    }

    public function getAlerts()
    {
        // Реализация определения ситуаций, требующих внимания
    }

    public static function saveData(array $data)
    {
        return self::create($data);
    }

    public static function deleteOldData(int $days = 7)
    {
        // Удаление старых записей
    }
}
