<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    use HasFactory;

    protected $fillable = [
        'ip_address',
        'user_agent',
        'url',
        'method',
        'status_code',
        'created_at',
        'updated_at',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($request) {
            $request->ip_address = request()->ip(); // Получаем IP-адрес текущего запроса
            $request->user_agent = request()->header('User-Agent'); // Получаем User-Agent текущего запроса
            $request->url = request()->fullUrl(); // Получаем полный URL текущего запроса
            $request->method = request()->method(); // Получаем HTTP-метод текущего запроса
            $request->status_code = 200; // Инициализируем статус код запроса как 200
        });
    }
}
