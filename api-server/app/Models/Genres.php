<?php

declare(strict_types=1);

namespace App\Models;

enum Genres: int
{
    case Pop = 1;
    case Rab = 2;
    case Rock = 4;
    case HipHop = 8;
}
