<?php

namespace App\Models;

/*use Illuminate\Database\Eloquent\Factories\HasFactory;*/
use Illuminate\Database\Eloquent\Model;

class MonitoringLog extends Model
{
    protected $table = 'monitoring_logs';
    protected $fillable = ['event_type', 'log'];

    protected $primaryKey = 'id';

    protected $timestamps = true;
 }
}
