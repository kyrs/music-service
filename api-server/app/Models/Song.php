<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Song extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'album_id',
        'duration',
    ];

    public function album()
    {
        return $this->belongsTo('App\Models\Album');
    }

    public function playlists()
    {
        return $this->belongsToMany('App\Models\Playlist', 'playlist_song');
    }

}
