<?php

namespace App\DTO\MappingSchemes;

use App\Utils\MappingSchemeItem;

class RatingMappingScheme
{
    public static function getScheme(): array
    {
        return [
            new MappingSchemeItem('score', 'score'),
            new MappingSchemeItem('user_id', 'userId'),
            new MappingSchemeItem('song_id', 'songId'),
        ];
    }
}
