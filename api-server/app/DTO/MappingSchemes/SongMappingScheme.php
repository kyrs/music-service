<?php

namespace App\DTO\MappingSchemes;

use App\Utils\MappingSchemeItem;

class SongMappingScheme
{
    public static function getScheme(): array
    {
        return [
            new MappingSchemeItem('title', 'name'),
            new MappingSchemeItem('artist', 'artist'),
            new MappingSchemeItem('duration', 'duration'),
            new MappingSchemeItem('genre', 'genre'),
            new MappingSchemeItem('album', 'album'),
        ];
    }
}
