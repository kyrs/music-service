<?php

namespace App\DTO\MappingSchemes;

use App\Utils\MappingSchemeItem;

class PlaylistMappingScheme
{
    public static function getScheme(): array
    {
        return [
            new MappingSchemeItem('name', 'name'),
            new MappingSchemeItem('description', 'description'),
            new MappingSchemeItem('songs', 'songs'),
        ];
    }
}
