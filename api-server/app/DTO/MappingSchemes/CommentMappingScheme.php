<?php

namespace App\DTO\MappingSchemes;

use App\Utils\MappingSchemeItem;

class CommentMappingScheme
{
    public static function getScheme(): array
    {
        return [
            new MappingSchemeItem('text', 'text'),
            new MappingSchemeItem('author', 'author'),
            new MappingSchemeItem('song_id', 'songId'),
        ];
    }
}
