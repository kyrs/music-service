<?php

namespace App\DTO\MappingSchemes;

use App\Utils\MappingSchemeItem;

class AlbumMappingScheme
{
    public static function getScheme(): array
    {
        return [
            new MappingSchemeItem('album_name', 'title'),
            new MappingSchemeItem('artist_name', 'artist'),
            new MappingSchemeItem('release_date', 'releaseDate'),
        ];
    }
}
