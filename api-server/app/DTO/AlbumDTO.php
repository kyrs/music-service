<?php

namespace App\DTO;

class AlbumDTO
{
    public string $title;
    public string $artist;
    public string $releaseDate;
    public array $songs;
    public int $userId;
    public int $albumId; 

    public function __construct(string $title, string $artist, string $releaseDate, array $songs, int $userId, int $albumId)
    {
        $this->title = $title;
        $this->artist = $artist;
        $this->releaseDate = $releaseDate;
        $this->songs = $songs;
        $this->userId = $userId;
        $this->albumId = $albumId;
    }
}

