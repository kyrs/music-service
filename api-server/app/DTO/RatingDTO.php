<?php

namespace App\DTO;

class RatingDTO
{
    public int $score;
    public string $userId;
    public string $songId;

    public function __construct(int $score, string $userId, string $songId)
    {
        $this->score = $score;
        $this->userId = $userId;
        $this->songId = $songId;
    }
}
