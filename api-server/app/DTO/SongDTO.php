<?php

namespace App\DTO;

class SongDTO
{
    public string $title;
    public string $artist;
    public string $duration;
    public string $genre;
    public string $album;

    public function __construct(string $title, string $artist, string $duration, string $genre, string $album)
    {
        $this->title = $title;
        $this->artist = $artist;
        $this->duration = $duration;
        $this->genre = $genre;
        $this->album = $album;
    }
}
