<?php

namespace App\DTO;

class CommentDTO
{
    public string $text;
    public string $author;
    public string $songId;

    public function __construct(string $text, string $author, string $songId)
    {
        $this->text = $text;
        $this->author = $author;
        $this->songId = $songId;
    }
}
