<?php

namespace App\DTO;

class PlaylistDTO
{
    public string $name;
    public string $description;
    public array $songs;

    public function __construct(string $name, string $description, array $songs)
    {
        $this->name = $name;
        $this->description = $description;
        $this->songs = $songs;
    }
}
