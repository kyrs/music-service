<?php

namespace App\Http\Controllers;

use App\DataAccess\Repositories\UserRepository;
use App\Http\Requests\LoginRequest;
use App\Services\AuthJwtService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function __construct(
        private readonly AuthJwtService $authJwtService,
        private readonly UserRepository $userRepository
    ) {
    }

    public function login(LoginRequest $request): JsonResponse
    {
        $credentials = $request->body();

        $user = $this->userRepository->getByEmail($credentials->email);
        if ($user === null || !Hash::check($credentials->password, $user->password)) {
            return response()->json(status: Response::HTTP_FORBIDDEN);
        }

        $token = $this->authJwtService->createToken($user, ['create-albums']);

        return $this->respondWithToken($token);
    }

    public function me(): JsonResponse
    {
        return response()->json($this->getAuthInfo());
    }

    public function logout(): JsonResponse
    {
        // todo: delete key from redis
        $authInfo = $this->getAuthInfo();

        $this->authJwtService->deleteTokenFromRedis($authInfo->token);

        return response()->json(['message' => 'Successfully logged out']);
    }

    public function refresh(): JsonResponse
    {
        $authInfo = $this->getAuthInfo();

        $token = $authInfo->token;

        return $this->respondWithToken($token);
    }

    protected function respondWithToken($token): JsonResponse
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'Bearer',
            'expires_in' => (int)config('auth.jwt.expired_after_sec'),
        ]);
    }
}
