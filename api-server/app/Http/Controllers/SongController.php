<?php

namespace App\Http\Controllers;

use App\Http\Requests\UploadSongRequest;
use App\Models\User;
use App\Services\AudioService;
use App\Utils\Mapper;
use Illuminate\Http\JsonResponse;
use App\Models\Song;


class SongController extends Controller
{
    public function __construct(private readonly AudioService $audioService)
    {
    }

    public function index()
    {
        $songs = Song::orderBy('created_at', 'desc')->take(5)->get();
        return response()->json($songs);
    }

    public function uploadSong(UploadSongRequest $request, int $albumId): JsonResponse
    {
        $body = $request->body();

        $songId = $this->audioService->saveAudio($albumId, $body->title, $body->file);

        return response()->json($songId);
    }

    public function getSongsOfAlbum(int $albumId): JsonResponse
    {
       /*$songs = $this->audioService->getSongsByAlbum($albumId);*/
       $songs = $this->audioService->getSongsByAlbumDTO($albumId);
    if ($songs) {
        return response()->json($songs);
       } else {
        return response()->json(['message' => 'Песни не найдены'], 404);
       }
    }

    public function searchSongs(string $query): JsonResponse
    {
       $songs = $this->audioService->searchSongs($query);
       return response()->json($songs);
    }

    public function showPlaylistsForSong($songId)

    {
       $song = Song::find($songId);
       if ($song) {
           $playlists = $song->playlists;
        // Преобразование коллекции в массив для удобства использования в JSON
           $playlistsArray = $playlists->toArray();
        return response()->json($playlistsArray);
        } else {
        return response()->json(['message' => 'Песня не найдена'], 404);
        }
    }

    public function deleteSong(int $songId): JsonResponse
    {
       $this->audioService->deleteSong($songId);
       return response()->json(['message' => 'Песня успешно удалена']);
    }
}
