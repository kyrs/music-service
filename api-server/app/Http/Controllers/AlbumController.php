<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\DataAccess\Repositories\AlbumRepository;
use App\Http\Requests\CreateAlbumRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request; // Добавлено для использования в методе updateAlbum

class AlbumController
{
    public function __construct(private readonly AlbumRepository $albumRepository)
    {
    }

    public function getAlbumById(int $id): JsonResponse
    {
        $album = $this->albumRepository->getAlbumDTOById($id);
        if ($album) {
            return response()->json($album);
        } else {
            return response()->json(['message' => 'Альбом не найден'], 404);
        }
    }

    public function index()
    {
        $albums = $this->albumRepository->getAlbumsDTO(); // Предполагается, что метод возвращает DTO для альбомов
        return response()->json($albums);
    }

    public function createAlbum(CreateAlbumRequest $request): JsonResponse
    {
        // Используем данные из запроса для создания AlbumDTO
        $albumDTO = new AlbumDTO(
            $request->get('title'),
            $request->get('artist'),
            $request->input('release_date'),
            $request->input('songs'), // Предполагается, что songs передаются в виде массива
            $request->user()->id, // Получаем userId из аутентифицированного пользователя
            $request->input('album_id') // Предполагается, что album_id передается в запросе
        );

        // Создаем альбом с использованием AlbumRepository
        $albumId = $this->albumRepository->createAlbum($albumDTO);

        return response()->json(['albumId' => $albumId]);
    }

    public function updateAlbum(Request $request, $id)
    {
        // Используем данные из запроса для обновления AlbumDTO
        $albumDTO = new AlbumDTO(
            $request->input('title'),
            $request->input('artist'),
            $request->input('release_date'),
            $request->input('songs'), // Предполагается, что songs передаются в виде массива
            $request->user()->id, // Получаем userId из аутентифицированного пользователя
            $id // Используем id из URL
        );

        // Обновляем альбом с использованием AlbumRepository
        $this->albumRepository->updateAlbum($albumDTO);

        return response()->json(['message' => 'Альбом обновлен']);
    }

    public function deleteAlbum(int $id): JsonResponse
    {
        $this->albumRepository->deleteAlbum($id);

        return response()->json(['message' => 'Альбом удален']);
    }
}

/*use App\DataAccess\Repositories\AlbumRepository;
use App\Http\Requests\CreateAlbumRequest;
use Illuminate\Http\JsonResponse;

class AlbumController
{
    public function __construct(private readonly AlbumRepository $albumRepository)
    {
    }

    public function getAlbumById(int $id): JsonResponse
    {
        /*$album = $this->albumRepository->getById($id);
        $album = $this->albumRepository->getAlbumDTOById($id);
    if ($album) {
        return response()->json($album);
        } else {
        return response()->json(['message' => 'Альбом не найден'], 404);
        }
    }

    public function index()
    {
        $albums = Album::orderBy('created_at', 'desc')->take(5)->get();
        return response()->json($albums);
    }

    public function createAlbum(CreateAlbumRequest $request): JsonResponse
    {
        $body = $request->body();

        $albumId = $this->albumRepository->createAlbum(1, $body->name, $body->genreId);

        return response()->json($albumId);
    }

    public function updateAlbum(Request $request, $id)
    {
        $album = Album::findOrFail($id);

        $album->update([
        'name' => $request->name,
        'category_id' => $request->category_id,
    ]);

         return response()->json($album);
    }

    public function deleteAlbum(int $id): JsonResponse
    {
        $this->albumRepository->deleteAlbum($id);

        return response()->json(true);
    }*/
