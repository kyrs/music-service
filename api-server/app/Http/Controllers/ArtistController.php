<?php

namespace App\Http\Controllers;

use App\DataAccess\Repositories\ArtistRepository;
use Illuminate\Http\Request;

class ArtistController extends Controller
{
    protected $artistRepository;

    public function __construct(ArtistRepository $artistRepository)
    {
        $this->artistRepository = $artistRepository;
    }

    /*public function index()
    {
        try {
            $artists = $this->artistRepository->getArtists(5); //  в репозитории добавить метод getArtists
            if ($artists) {
                return response()->json($artists);
            } else {
                return response()->json(['message' => 'Данные не найдены'], 404);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'Ошибка при получении данных'], 500);
        }
    }*/

    /*public function addAlbumToArtist(Request $request, $artistId)
    {
        $albumId = $request->input('album_id');

        if ($this->artistRepository->addAlbumToArtist($artistId, $albumId)) {
            return response()->json(['message' => 'Альбом успешно добавлен'], 200);
        } else {
            return response()->json(['message' => 'Ошибка при добавлении альбома'], 500);
        }
    }*/
    public function addAlbumToArtist(Request $request, $artistId)
    {
      $albumMapper = new AlbumMapper();
      $albumDTO = $albumMapper->map($request);

      if ($this->artistRepository->addAlbumToArtist($artistId, $albumDTO->albumId)) {
          return response()->json(['message' => 'Альбом успешно добавлен'], 200);
      } else {
          return response()->json(['message' => 'Ошибка при добавлении альбома'], 500);
      }
    }

    public function deleteAlbumFromArtist($artistId, $albumId)
    {
        if ($this->artistRepository->deleteAlbumFromArtist($artistId, $albumId)) {
            return response()->json(['message' => 'Альбом успешно удален'], 200);
        } else {
            return response()->json(['message' => 'Ошибка при удалении альбома'], 500);
        }
    }

    /*public function addSongToAlbum(Request $request, $artistId, $albumId)
    {
        $trackData = $request->only(['title', 'duration']);

        if ($this->artistRepository->addSongToAlbum($artistId, $albumId, $trackData)) {
            return response()->json(['message' => 'Песня успешно добавлена'], 200);
        } else {
            return response()->json(['message' => 'Ошибка при добавлении песни'], 500);
        }
    }*/
    public function addSongToAlbum(Request $request, $artistId, $albumId)
    {
       $songMapper = new SongMapper();
       $songDTO = $songMapper->map($request);

       if ($this->artistRepository->addSongToAlbum($artistId, $albumId, $songDTO)) {
          return response()->json(['message' => 'Песня успешно добавлена'], 200);
        } else {
          return response()->json(['message' => 'Ошибка при добавлении песни'], 500);
        }
    }

    public function removeSongFromAlbum($artistId, $albumId, $trackId)
    {
        if ($this->artistRepository->removeSongFromAlbum($artistId, $albumId, $trackId)) {
            return response()->json(['message' => 'Песня успешно удалена'], 200);
        } else {
            return response()->json(['message' => 'Ошибка при удалении песни'], 500);
        }
    }
}
