<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\DataAccess\Repositories\PlaylistRepository;
use App\Http\Requests\CreatePlaylistRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PlaylistController
{
    public function __construct(private readonly PlaylistRepository $playlistRepository)
    {
    }

    public function show(Playlist $playlist)
    {
            return view('playlists.show', ['playlist' => $playlist]);
    }

    /*public function index()
    {
        try {
            $playlists = $this->playlistRepository->getPlaylists(5); // Предполагается, что в репозитории есть метод getPlaylists
            if ($playlists) {
                return response()->json($playlists);
            } else {
                return response()->json(['message' => 'Данные не найдены'], 404);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'Ошибка при получении данных'], 500);
        }
    }*/

    public function getPlaylistById(int $id): JsonResponse
    {
        $playlist = $this->playlistRepository->getById($id);
        if ($playlist) {
            return response()->json($playlist);
        } else {
            return response()->json(['message' => 'Плейлист не найден'], 404);
        }
    }

    public function createPlaylist(CreatePlaylistRequest $request): JsonResponse
    {
        $body = $request->body();

        $playlistId = $this->playlistRepository->createPlaylist($body->userId, $body->name, $body->description);

        return response()->json($playlistId);
    }

    public function updatePlaylist(Request $request, $id)
    {
        $playlist = Playlist::findOrFail($id);

        $playlist->update([
            'name' => $request->name,
            'description' => $request->description,
        ]);

        return response()->json($playlist);
    }

    public function deletePlaylist(int $id): JsonResponse
    {
        $this->playlistRepository->deletePlaylist($id);

        return response()->json(true);
    }
}
