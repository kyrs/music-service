<?php

namespace App\Http\Controllers;

use App\DataAccess\Repositories\UserRepository;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /*public function addAlbum(Request $request)
    {
        $userId = $request->user()->id;
        $albumId = $request->input('album_id');

        if ($this->userRepository->addAlbumToUser($userId, $albumId)) {
            return response()->json(['message' => 'Альбом успешно добавлен'], 200);
        } else {
            return response()->json(['message' => 'Ошибка при добавлении альбома'], 500);
        }
    }*/

    public function addAlbum(Request $request)
{
    $userId = $request->user()->id;
    $albumData = $request->only(['album_name', 'artist_name', 'release_date', 'songs']);

    // Преобразование данных в соответствии с AlbumMappingScheme
    $mappingScheme = AlbumMappingScheme::getScheme();
    $albumDetails = [];
    foreach ($mappingScheme as $item) {
        $albumDetails[$item->getTarget()] = $albumData[$item->getSource()];
    }

    // Создание AlbumDTO
    $albumDTO = new AlbumDTO(
        $albumDetails['title'],
        $albumDetails['artist'],
        $albumDetails['releaseDate'],
        $albumData['songs'] // Предполагается, что 'songs' уже в нужном формате
    );

    if ($this->userRepository->addAlbumToUser($userId, $albumDTO)) {
        return response()->json(['message' => 'Альбом успешно добавлен'], 200);
    } else {
        return response()->json(['message' => 'Ошибка при добавлении альбома'], 500);
    }
}

}

