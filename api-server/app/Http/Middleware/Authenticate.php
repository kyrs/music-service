<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class Authenticate
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     */
    function handle(Request $request, Closure $next)
    {
        if ($request->get('authInfo') === null) {
            return new Response(status: Response::HTTP_UNAUTHORIZED);
        }

        return $next($request);
    }
}
