<?php

namespace App\Http\Middleware;

use App\Models\Auth\AuthJwtModel;
use App\Services\AuthJwtService;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class AuthInfoMiddleware
{
    public function __construct(private readonly AuthJwtService $authJwtService)
    {
    }

    /**
     * Handle an incoming request.
     *
     * @param  Request $request
     * @param  Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $request->attributes->add(['authInfo' => $this->getAuthInfo($request)]);

        return $next($request);
    }

    private function getAuthInfo(Request $request): AuthJwtModel|null
    {
        $header = $request->header('Authorization');

        if ($header === null) {
            return null;
        }

        $parts = explode(' ', $header, 2);

        if (count($parts) < 2) {
            return null;
        }

        [$authHeader, $authToken] = $parts;

        if ($authHeader !== 'Bearer') {
            return null;
        }

        // todo: check the token in redis
        $decodedToken = $this->authJwtService->decodeToken($authToken);
        if ($decodedToken === null) {
            return null;
        }

        $redisKey = "jwt:{$decodedToken->userId}:{$decodedToken->exp}";
        if (!Redis::exists($redisKey)) {
            return null;
        }

        return $decodedToken;

        // return $this->authJwtService->decodeToken($authToken);
    }
}
