<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\Models\Auth\AuthJwtModel;
use Closure;
use Illuminate\Http\Request;

class HavePermission
{
    public function handle(Request $request, Closure $next, ...$permissionCodes)
    {
        /** @var AuthJwtModel|null $authInfo */
        $authInfo = $request->get('authInfo');

        if ($authInfo === null && count($permissionCodes) > 0) {
            return response(status: 403);
        }

        foreach ($permissionCodes as $permissionCode) {
            if (!in_array($permissionCode, $authInfo->permissions, true)) {
                return response(status: 403);
            }
        }

        return $next($request);
    }

    public static function permissions(string ...$permissionCodes): string
    {
        $permissionsStr = implode(',', $permissionCodes);

        return self::class . ':' . $permissionsStr;
    }
}
