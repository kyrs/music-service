<?php

declare(strict_types=1);

namespace App\Http\RequestModels;

class LoginModel
{
    public string $email;

    public string $password;
}
