<?php

declare(strict_types=1);

namespace App\Http\RequestModels;

class CreateAlbumModel
{
    public string $name;

    public int $genreId;
}
