<?php

declare(strict_types=1);

namespace App\Http\RequestModels;

use Illuminate\Http\UploadedFile;

class UploadSongModel
{
    public string $title;

    public UploadedFile $file;
}
