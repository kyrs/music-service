<?php

declare(strict_types=1);

namespace App\Http\Requests;

use App\Http\RequestModels\LoginModel;

class LoginRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'email' => ['required', 'email'],
            'password' => ['required', 'string'],
        ];
    }

    public function body(): LoginModel
    {
        return $this->innerBodyObject(new LoginModel());
    }
}
