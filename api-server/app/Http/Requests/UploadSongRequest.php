<?php

namespace App\Http\Requests;

use App\Http\RequestModels\UploadSongModel;

class UploadSongRequest extends BaseFormRequest
{
    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            'title' => ['required', 'string'],
            'file' => ['required', 'mimes:mp3'],
        ];
    }

    public function body(): UploadSongModel
    {
        $model = new UploadSongModel();
        $model->title = $this->string('title');
        $model->file = $this->file('file');

        return $model;
    }
}
