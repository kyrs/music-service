<?php

declare(strict_types=1);

namespace App\Http\Requests;

use App\Http\RequestModels\CreateAlbumModel;

class CreateAlbumRequest extends BaseFormRequest
{
    public function body(): CreateAlbumModel
    {
        return $this->innerBodyObject(new CreateAlbumModel());
    }
}
