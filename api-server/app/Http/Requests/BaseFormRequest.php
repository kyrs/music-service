<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use JsonMapper;

abstract class BaseFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [];
    }

    abstract public function body(): mixed;

    /*protected function innerBodyObject(mixed $body): mixed
    {
        $jsonMapper = new JsonMapper();
        $jsonMapper->bEnforceMapType = false;

        return $jsonMapper->map($this->input(), $body);
    }*/

    protected function innerBodyObject(mixed $body): mixed
{
    $jsonMapper = new JsonMapper();
    $jsonMapper->bEnforceMapType = false;

    // Используйте $this->all() для получения всех входных данных запроса
    return $jsonMapper->map($this->input(), $body);
}

}
