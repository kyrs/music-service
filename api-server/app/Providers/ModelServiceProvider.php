<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\MonitoringLog;
use App\Models\MonitoringResult;

class ModelServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the MonitoringResult and MonitoringLog models.
     */
    public function registerModels()
    {
        $this->app->bind(MonitoringResult::class);
        $this->app->bind(MonitoringLog::class);
    }
}

