<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Artisan;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\MonitorRequests::class,
        Commands\MonitorApiResponseTime::class,
        Commands\MonitorActiveUsers::class,
        Commands\MonitorErrors::class,
    ];

    protected function schedule(Schedule $schedule): void
    {
        // Определение команд с разными приоритетами
        $highPriorityCommands = [
            \App\Console\Commands\MonitorRequests::class,
            \App\Console\Commands\MonitorErrors::class,
        ];
        $mediumPriorityCommands = [
            \App\Console\Commands\MonitorApiResponseTime::class,
        ];
        $lowPriorityCommands = [
            \App\Console\Commands\MonitorActiveUsers::class,
        ];

        // Расписание для команд высокого приоритета
        $nextRunTime = now()->setTime(12, 0);
        foreach ($highPriorityCommands as $command) {
            $schedule->command($command)->everyHalfHour()
                     ->between('12:00', '22:00')
                     ->startingOn($nextRunTime)
                     ->withoutOverlapping();
            $nextRunTime->addHour();
        }

        // Расписание для команд среднего приоритета
        $nextRunTime = now()->setTime(12, 0);
        foreach ($mediumPriorityCommands as $command) {
            $schedule->command($command)->hourly()
                     ->between('12:00', '22:00')
                     ->startingOn($nextRunTime)
                     ->withoutOverlapping();
            $nextRunTime->addHour();
        }

        // Расписание для команд низкого приоритета
        $nextRunTime = now()->setTime(12, 0);
        foreach ($lowPriorityCommands as $command) {
            $schedule->command($command)->daily()
                     ->between('12:00', '22:00')
                     ->startingOn($nextRunTime)
                     ->withoutOverlapping();
            $nextRunTime->addHour();
        }
    }

    /**
     * Register the commands for the application.
     */
    protected function commands(): void
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
