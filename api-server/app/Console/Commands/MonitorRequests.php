<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Request;
use Illuminate\Support\Facades\Log;

class MonitorRequests extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:monitor-requests';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Displays the number of requests processed in the last hour';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        // Получаем количество запросов за последний час
        $lastHourRequestsCount = Request::where('created_at', '>=', now()->subHour())->count();

        // Выводим результат
        $this->info("Number of requests processed in the last hour: {$lastHourRequestsCount}");

        Log::info('Это информационное сообщение');
        Log::warning('Это предупреждение');
        Log::error('Это сообщение об ошибке');
    }
}
