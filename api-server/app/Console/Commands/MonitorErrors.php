<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class MonitorErrors extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:monitor-errors';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Monitors application errors';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        // Путь к файлу лога ошибок
        $logPath = storage_path('logs/laravel.log');

        // Читаем логи
        $logs = file($logPath);

        // Проходимся по всем строкам лога
        foreach ($logs as $line) {
            // Проверяем, содержит ли строка слово "error"
            if (strpos($line, 'error')!== false) {
                // Выводим строку в консоль
                $this->info($line);
            }
        }

        Log::info('Это информационное сообщение');
        Log::warning('Это предупреждение');
        Log::error('Это сообщение об ошибке');

    }
}
