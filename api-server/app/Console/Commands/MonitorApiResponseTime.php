<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class MonitorApiResponseTime extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:monitor-api-response-time';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Monitors the response time of your API endpoint';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        // URL вашего API-эндпоинта
        $apiEndpoint = 'http://your-api-endpoint.com/api/data';

        // Количество запросов для отправки
        $requestCount = 2;

        // Счетчик для хранения времени ответа
        $responseTimes = [];

        // Отправляем запросы и измеряем время ответа
        for ($i = 0; $i < $requestCount; $i++) {
            $start = microtime(true);
            $response = Http::get($apiEndpoint);
            $end = microtime(true);

            $responseTimes[] = $end - $start;
        }

        // Вычисляем среднее время ответа
        $averageResponseTime = array_sum($responseTimes) / count($responseTimes);

        // Выводим результат
        $this->info("Average response time for the API endpoint: {$averageResponseTime} seconds");

        Log::info('Это информационное сообщение');
        Log::warning('Это предупреждение');
        Log::error('Это сообщение об ошибке');
    }
}
