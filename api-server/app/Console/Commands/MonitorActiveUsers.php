<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use Illuminate\Support\Facades\Log;

class MonitorActiveUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:monitor-active-users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Displays the number of active users in the last 24 hours';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        // Определяем время начала периода, за который будем считать активных пользователей
        $startTime = now()->subDay();

        // Подсчитываем количество пользователей, которые были активными в последнее время
        $activeUsersCount = User::where('last_activity', '>=', $startTime)->count();

        // Выводим результат
        $this->info("Number of active users in the last 24 hours: {$activeUsersCount}");

        Log::info('Тестовое сообщение info от cron-задачи MonitorActiveUsers');
        Log::warning('Тестовое сообщение warning от cron-задачи MonitorActiveUsers');
        Log::error('Тестовое сообщение error от cron-задачи MonitorActiveUsers');
    }
}
