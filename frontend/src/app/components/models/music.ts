export interface Album {
    id: number;
    artistId: number;
    name: string;
    date: Date;
}

export interface Song {
    id: number;
    title: string;
    src?: string;
    duration: number;

    added: boolean;
}
