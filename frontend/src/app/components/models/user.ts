export interface UserInfo {
    userId: number;
    name: string;
    email: string;
    permissions: string[];
}

export interface AuthToken {
  accessToken: string;
  tokenType: string;
  expiresIn: number;
}
