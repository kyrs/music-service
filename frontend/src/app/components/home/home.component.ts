import { Component, OnInit } from '@angular/core';
import { Album } from '../models/music';
import { MusicLibraryService } from 'src/app/services/music-library.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public albums: Album[] = [];

  constructor(private musicService: MusicLibraryService) {}

  public ngOnInit(): void {
    this.musicService.getTopAlbums().subscribe(data => this.albums = data);
  }
}
