import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { AuthState } from 'src/app/services/auth.state';
import {UserService} from "../../services/user.service";
import {UserInfo} from "../models/user";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  public form: FormGroup;

  constructor(
    private authService: AuthService,
    private authState: AuthState,
    private userService: UserService,
    private router: Router
  ) {
    this.form = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required])
    });
  }

  public get emailControl() {
    return this.form.get('email');
  }

  public get passwordControl() {
    return this.form.get('password');
  }

  public submit(): void {
    if (this.form.valid) {
      this.authService.login(this.form.value).subscribe(authInfo => {
        this.authState.setAccessToken(authInfo.accessToken);

        this.userService.getCurrentUser().subscribe(
          (userInfo: UserInfo) => {
            this.authState.setUserInfo(userInfo);
            this.router.navigateByUrl('/');
          });
      });
    }
  }
}
