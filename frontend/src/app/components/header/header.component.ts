import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { AuthState } from 'src/app/services/auth.state';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  public isLoggedIn: boolean = false;
  public name: string|null = null;

  constructor(
    private authState: AuthState,
    private authService: AuthService,
    private router: Router
  ) {
    this.authState.getUserInfo().subscribe(authInfo => {
      if (authInfo !== null) {
        this.name = authInfo.name;
        this.isLoggedIn = true;
      } else {
        this.name = null;
        this.isLoggedIn = false;
      }
    })
  }

  public logout(): void {
    this.authService.logout().subscribe(() => {
      this.authState.setUserInfo(null);
      this.authState.clearAccessToken();
      this.router.navigateByUrl('/login');
    });
  }
}
