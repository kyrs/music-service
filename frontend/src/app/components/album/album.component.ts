import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Album, Song } from '../models/music';
import { MusicLibraryService } from 'src/app/services/music-library.service';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.scss']
})
export class AlbumComponent {

  public album: Album;

  public songs: Song[];

  constructor(private route: ActivatedRoute, private musicLibraryService: MusicLibraryService) {
    this.album = this.route.snapshot.data['album'];
    this.songs = this.route.snapshot.data['albumSongs'];
  }

  public addOrRemoveSong(song: Song): void {
    if (song.added) {
      this.musicLibraryService.removeSong(song.id).subscribe(() => song.added = false);
    } else {
      this.musicLibraryService.addSong(song.id).subscribe(() => song.added = true);
    }
  }
}
