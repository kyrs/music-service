import { Component } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent {
  searchQuery: string = '';

  search() {
     // Здесь реализуйте логику поиска
     console.log(this.searchQuery);
  }
 }
