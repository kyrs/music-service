import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { AlbumComponent } from './components/album/album.component';
import { SearchComponent } from './components/search/search.component';
import { albumResolver, albumSongsResolver } from './services/resolvers/album.resolver';
import { authGuard } from './services/auth.guard';
import { LibraryComponent } from './components/library/library.component';
import { PlayerComponent } from './components/player/player.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  {
    path: '',
    component: HomeComponent,
    canActivate: [authGuard]
  },
  {
    path: 'albums/:id',
    component: AlbumComponent,
    canActivate: [authGuard],
    resolve: { album: albumResolver, albumSongs: albumSongsResolver }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
