import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthToken } from '../components/models/user';
import {RestService} from "./rest.service";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private restService: RestService) { }

  public login(authInfo: { email: string, password: string}): Observable<AuthToken> {
    return this.restService.post<AuthToken>('/api/auth/login', authInfo);
  }

  public logout(): Observable<any> {
    return this.restService.post('/api/auth/logout', {});
  }
}
