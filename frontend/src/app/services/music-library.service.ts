import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Album, Song } from '../components/models/music';
import {RestService} from "./rest.service";

@Injectable({
  providedIn: 'root'
})
export class MusicLibraryService {

  constructor(private restService: RestService) { }

  public getTopAlbums(): Observable<Album[]> {
    return this.restService.get<Album[]>('/api/albums/top');
  }

  public getAlbum(albumId: number): Observable<Album> {
    return this.restService.get<Album>(`/api/albums/${albumId}`);
  }

  public getAlbumSongs(albumId: number): Observable<Song[]> {
    return this.restService.get<Song[]>(`/api/albums/${albumId}/songs`);
  }

  public addSong(songId: number): Observable<any> {
    return this.restService.post<any>(`/api/add/${songId}`, {});
  }

  public removeSong(songId: number): Observable<any> {
    return this.restService.post<any>(`/api/remove/${songId}`, {});
  }
}
