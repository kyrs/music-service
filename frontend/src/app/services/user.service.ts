import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserInfo } from '../components/models/user';
import {RestService} from "./rest.service";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private restService: RestService) { }

  public getCurrentUser(): Observable<UserInfo> {
    return this.restService.get<UserInfo>('/api/auth/me');
  }
}
