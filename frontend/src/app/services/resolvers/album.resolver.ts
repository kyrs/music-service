import { inject } from "@angular/core";
import { ActivatedRouteSnapshot, ResolveFn, RouterStateSnapshot } from "@angular/router";
import { Album, Song } from "src/app/components/models/music";
import { MusicLibraryService } from "../music-library.service";

export const albumResolver: ResolveFn<Album> =
    (route: ActivatedRouteSnapshot, state: RouterStateSnapshot) => {
        return inject(MusicLibraryService).getAlbum(+route.paramMap.get('id')!);
    };

export const albumSongsResolver: ResolveFn<Song[]> =
    (route: ActivatedRouteSnapshot, state: RouterStateSnapshot) => {
        return inject(MusicLibraryService).getAlbumSongs(+route.paramMap.get('id')!);
    };