import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {AuthState} from "./auth.state";

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private http: HttpClient, private authState: AuthState) { }

  public get<T>(url: string): Observable<T> {
    return this.http.get<T>(url, { headers: this.getDefaultHeaders() });
  }

  public post<T>(url: string, data: any): Observable<T> {
    return this.http.post<T>(url, data, { headers: this.getDefaultHeaders() });
  }

  private getDefaultHeaders(): HttpHeaders {
    return new HttpHeaders({
      'Authorization': `Bearer ${this.authState.getAccessToken()}`
    });
  }
}
